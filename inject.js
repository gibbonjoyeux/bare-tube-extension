/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

"use strict";

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	MAX_TITLE_TRY 		= 20;
const	TITLE_RETRY_DELAY	= 500;

let		g_node;
let		g_quality_levels;
let		g_duration;
let		g_title;
let		g_title_try_count;
let		g_channel;

///
/// USER DATA
///
/// QUALITY:
///  - max
///  - ...
///  - 1080
///  - 720
///  - 360
///  - 240
///  - 144
///  - (min)
///  - auto
///
/// CONDITION TYPES: (with invert option)
///  - contains
///  - contains_strict
///  - channel_match
///  - shorter_than
///  - longer_than
///  - regexp
///
/// LOGICAL TYPES:
///  - OR
///  - AND
///

let		data = {
	"default": "medium",
	"rules": [
		{
			"quality": "tiny",
			"logical": "AND",
			"conditions": [
				{
					"type": "shorter_than",
					"param": 6 * 60
				},
				{
					"type": "contains",
					"param": "trailer",
					"invert": true
				}
			]
		}
		//{
		//	"quality": "medium",
		//	"conditions": [
		//		{
		//			"type": "contains",
		//			"param": "trailer"
		//		}
		//	]
		//},
		//{
		//	"quality": "medium",
		//	"conditions": [
		//		{
		//			"type": "channel_matches",
		//			"param": "The Coding Train"
		//		}
		//	]
		//}
	]
};

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function wait(ms) {
	return new Promise(resolve => { setTimeout(() => { resolve(ms); }, ms); });
} 

//////////////////////////////////////////////////
/// LOAD DATA
//////////////////////////////////////////////////

async function	get_title() {
	g_title = document.querySelector("h1.title.style-scope.ytd-video-primary-info-renderer");
	if (g_title == undefined) {
		++g_title_try_count;
		if (g_title_try_count < MAX_TITLE_TRY) {
			await wait(TITLE_RETRY_DELAY);
			await get_title();
		}
	} else {
		/// GET TITLE
		g_title = g_title.textContent;
		/// GET CHANNEL
		g_channel = document.querySelector("ytd-channel-name#channel-name");
		g_channel = g_channel.querySelector("a").textContent;
	}
}

function	get_node() {
	g_node = document.querySelector(".html5-video-player");
}

function	get_duration() {
	g_duration = g_node.getDuration();
}

function	get_quality_levels() {
	if (g_node.getAvailableQualityLevels) {
		g_quality_levels = g_node.getAvailableQualityLevels();
	}
}

//////////////////////////////////////////////////
/// COMPUTATION
//////////////////////////////////////////////////

//////////////////////////////
/// CONDITIONS
//////////////////////////////

function	condition_contains(param) {
	let		title;

	title = g_title.toLowerCase();
	param = param.toLowerCase();
	return (title.includes(param));
}

function	condition_contains_strict(param) {
	return (g_title.includes(param));
}

function	condition_channel_matches(param) {
	return (g_channel == param);
}

function	condition_shorter_than(param) {
	return (g_duration <= param);
}

function	condition_longer_than(param) {
	return (g_duration >= param);
}

function	condition_regexp(param) {
	let		regexp;

	regexp = new RegExp(param, "i");
	return (g_title.match(regexp) != null);
}

function	condition_regexp_strict(param) {
	let		regexp;

	regexp = new RegExp(param);
	return (g_title.match(regexp) != null);
}

//////////////////////////////
/// MAIN
//////////////////////////////

function	set_quality(quality) {
	if (quality == "max") {
		quality = g_quality_levels[0];
	}
	g_node.setPlaybackQualityRange(quality);
	g_node.setPlaybackQuality(quality);
}

function	compute_rules() {
	let		quality;
	let		rule, condition;
	let		logical;
	let		func;
	let		state;

	quality = data.default;
	/// FOR EACH RULE
	for (rule of data.rules) {
		logical = rule.logical || "OR";
		/// FOR EACH CONDITION
		for (condition of rule.conditions) {
			console.log(condition);
			/// GET CONDITION RESULT
			state = window["condition_" + condition.type](condition.param);
			console.log("->", state, "(" + condition.invert + ")");
			if (condition.invert == true) {
				state = !state;
			}
			console.log("=>", state);
			/// TRUE
			if (state == true) {
				if (logical == "OR") {
					break;
				}
			/// FALSE
			} else if (logical == "AND") {
				break;
			}
		}
		console.log("state:", state);
		/// CHECK RULE STATE
		if (state == true) {
			quality = rule.quality;
			break;
		}
	}
	console.log("quality:", quality);
	/// SET QUALITY
	set_quality(quality);
}

async function	run() {
	g_title_try_count = 0;
	/// RETRIEVE DATA
	get_node();
	await get_title();
	get_duration();
	get_quality_levels();
	console.log(g_title);
	console.log(g_duration);
	console.log(g_quality_levels);
	/// COMPUTE RULES
	compute_rules();
}

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

run();
