/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// DEFAULT QUALITY
//////////////////////////////////////////////////

function		update_default_quality(dom_default, default_quality) {
	console.log(default_quality);
	dom_default.value = default_quality;
	browser.storage.local.set({"default": default_quality});
}

function		handle_default_quality(default_quality) {
	let			dom_default;

	dom_default = document.getElementById("default_quality");
	update_default_quality(dom_default, default_quality);
	dom_default.addEventListener("change", function () {
		default_quality = dom_default.value;
		update_default_quality(dom_default, default_quality);
	});
}

//////////////////////////////////////////////////
/// MODE
//////////////////////////////////////////////////

function		update_mode(dom_mode, mode) {
	dom_mode.value = (mode) ? "ON" : "OFF";
	browser.storage.local.set({"mode": mode});
}

function		handle_mode(mode) {
	let			dom_mode;

	dom_mode = document.getElementById("b_mode");
	update_mode(dom_mode, mode);
	dom_mode.addEventListener("click", function () {
		mode = !mode;
		update_mode(dom_mode, mode);
	});
	dom_mode.addEventListener("mouseover", function () {
		dom_mode.value = (!mode) ? "ON" : "OFF";
	});
	dom_mode.addEventListener("mouseout", function () {
		dom_mode.value = (mode) ? "ON" : "OFF";
	});
}

//////////////////////////////////////////////////
/// RUN
//////////////////////////////////////////////////

document.addEventListener('DOMContentLoaded', function() {
	/// HANDLE QUALITY & MODE
	browser.storage.local.get(["default", "mode"], function (data) {
		/// HANDLE DEFAULT VALUE
		if (!("mode" in data)) { data.mode = true; }
		if (!("default" in data)) { data.default = "auto"; }
		/// HANDLE EVENTS
		handle_mode(data.mode);
		handle_default_quality(data.default);
	});
	/// HANDLE DASHBOARD
	document.getElementById("b_dashboard").addEventListener("click",
	function () {
		browser.tabs.create({"url": "./dashboard/index.html"}, null);
	});
});
