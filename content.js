/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

browser.storage.local.get(["mode"], function (data) {
	let	script;
	
	/// CHECK MODE
	if ("data" in mode) {
		if (data.mode == false) { return; }
	/// INIT EXTENSION
	} else {
		browser.storage.local.set({
			"mode": true,
			"default": "auto",
			"rules": []
		});
	}
	/// LOAD <inject.js>
	script = document.createElement('script');
	script.src = browser.runtime.getURL('inject.js');
	script.onload = function() {
		this.remove();
	};
	/// INJECT <inject.js>
	(document.head || document.documentElement).appendChild(script);
});
